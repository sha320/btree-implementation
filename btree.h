//struc we usin out here
typedef struct btNode *bTree;
//make the tree
bTree btCreate(void);

//destory the tree (or free)
void btDestroy(bTree t);

//search tree by key
int btSearch(bTree t, int key);

//insert in the tree by key
void btInsert(bTree t, int key);

//printing key values
void btPrintKeys(bTree t);