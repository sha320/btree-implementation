//sources include https://gist.github.com/yorickdewid/d86e14cb2f3929823841
// https://sites.google.com/site/itstudentjunction/lab-programming-solutions/data-structures-programs/trees--programs---data-structures/program-for-implementation-of-b-tree-insertion--deletion
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "btree.h"

#define KEYS (1024) //max number of keys

struct Node {
    int isLeaf;     //check if leaf node
    int numKeys;    //key values
    int keys[KEYS];
    struct Node *kids[KEYS+1];  // kids[] holds nodes
};

bTree
Create(void)
{
    bTree biddy;

    biddy = malloc(sizeof(*b));
    assert(b);

    biddy->isLeaf = 1;
    biddy->numKeys = 0;

    return biddy;
}

void
Destroy(bTree b)
{
    int i;

    if(!b->isLeaf) { //if not a leaf, get rid of the kids
        for(i = 0; i < b->numKeys + 1; i++) {
            Destroy(b->kids[i]);
        }
    }

    free(b);
}

//look through the sort and find the lowest index to return 
//helper method for search
static int
keySearch(int n, const int *a, int key)
{
    int low;
    int high;
    int mid;

    low = -1;
    high = n;

    while(low + 1 < high) {
        mid = (low+high)/2;
        if(a[mid] == key) {
            return mid;
        } else if(a[mid] < key) {
            low = mid;
        } else {
            high = mid;
        }
    }

    return high;
}

int
Search(bTree b, int key)
{
    int position;

    //is tree empty?
    if(b->numKeys == 0) {
        return 0;
    }

    
    position = keySearch(b->numKeys, b->keys, key);

    if(position < b->numKeys && b->keys[position] == key) {
        return 1;
    } else {
        return(!b->isLeaf && btSearch(b->kids[position], key));
    }
}
//putting a key into tree
/* becomes right sibling if the node has to split */
//helper method for INSERT
static bTree
InsertHelper(bTree b, int key, int *median)
{
    int pos;
    int mid;
    bTree b2;

    pos = keySearch(b->numKeys, b->keys, key);

    if(pos < b->numKeys && b->keys[pos] == key) {
        return 0;
    }

    if(b->isLeaf) {

        memmove(&b->keys[pos+1], &b->keys[pos], sizeof(*(b->keys)) * (b->numKeys - pos)); //moves all elements up one
        b->keys[pos] = key;
        b->numKeys++;

    } else {
        //go into child node
        b2 = btInsertHelper(b->kids[pos], key, &mid);
        
        if(b2) {
            memmove(&b->keys[pos+1], &b->keys[pos], sizeof(*(b->keys)) * (b->numKeys - pos));
            //the new child is in position+1
            memmove(&b->kids[pos+2], &b->kids[pos+1], sizeof(*(b->keys)) * (b->numKeys - pos));
            b->keys[pos] = mid;
            b->kids[pos+1] = b2;
            b->numKeys++;
        }
    }

    if(b->numKeys >= KEYS) {
        mid = b->numKeys/2;

        *median = b->keys[mid];

        b2 = malloc(sizeof(*b2));

        b2->numKeys = b->numKeys - mid - 1;
        b2->isLeaf = b->isLeaf;
        memmove(b2->keys, &b->keys[mid+1], sizeof(*(b->keys)) * b2->numKeys);
        if(!b->isLeaf) {
            memmove(b2->kids, &b->kids[mid+1], sizeof(*(b->kids)) * (b2->numKeys + 1));
        }

        b->numKeys = mid;

        return b2;
    } else {
        return 0;
    }
}

void
Insert(bTree b, int key)
{
    bTree leftChild;  
    bTree rightChild;   
    int median;

    rightChild = btInsertHelper(b, key, &median);

    if(rightChild) {

        leftChild = malloc(sizeof(*leftChild));
        assert(leftChild);

      
        memmove(leftChild, b, sizeof(*b));

       
        b->numKeys = 1;
        b->isLeaf = 0;
        b->keys[0] = median;
        b->kids[0] = leftChild;
        b->kids[1] = rightChild;
    }
}